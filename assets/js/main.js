import {
  buildSetBookDataActDialog,
  actDialog,
  buildBoxedListItems,
} from "./ui.js";

import {
  loadBookList,
  delBookData,
  loadActivePage,
  saveActivePage,
  activePage,
  completedBookList,
  uncompletedBookList,
} from "./storage.js";

const boxedListCont = document.getElementById("boxed-list");
const swNotComplete = document.getElementById("sw-uncompleted-page");
const swCompleted = document.getElementById("sw-completed-page");
const warnNoData = document.getElementById("warning-no-data");

// load prev data if any
window.addEventListener("load", function (ev) {
  loadBookList();

  // load active page state
  loadActivePage();
  if (activePage == "uncomplete") {
    buildBoxedListItems();

    swCompleted.classList.remove("bg-l2-grey");
    swNotComplete.classList.remove("bg-l1-grey");
    swNotComplete.classList.add("bg-l2-grey");
  } else {
    buildBoxedListItems();

    swNotComplete.classList.remove("bg-l2-grey");
    swCompleted.classList.remove("bg-l1-grey");
    swCompleted.classList.add("bg-l2-grey");
  }

  if (completedBookList.length == 0 && uncompletedBookList.length == 0) {
    boxedListCont.style.display = "none";
    warnNoData.style.display = "block";
  }
});

const addBookButton = document.getElementById("add-book");
addBookButton.addEventListener("click", function (ev) {
  buildSetBookDataActDialog("add");
  actDialog.style.display = "block";
});

const clearBookButton = document.getElementById("clear-book");
clearBookButton.addEventListener("click", function (ev) {
  delBookData();
});

// display data
swNotComplete.addEventListener("click", function (ev) {
  swCompleted.classList.remove("bg-l2-grey");
  swNotComplete.classList.remove("bg-l1-grey");
  swNotComplete.classList.add("bg-l2-grey");
  saveActivePage("uncomplete");
  loadActivePage();

  buildBoxedListItems();
});

swCompleted.addEventListener("click", function (ev) {
  swNotComplete.classList.remove("bg-l2-grey");
  swCompleted.classList.remove("bg-l1-grey");
  swCompleted.classList.add("bg-l2-grey");
  saveActivePage("complete");
  loadActivePage();

  buildBoxedListItems();
});
