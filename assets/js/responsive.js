import {
  createImgElm,
  createTextForElm,
  actDialog,
  boxedListCont,
} from "./ui.js";

const winMatchMed = window.matchMedia("(min-width: 700px)");
const swUncompleted = document.getElementById("sw-uncompleted-page");
const swCompleted = document.getElementById("sw-completed-page");
const warnNoData = document.getElementById("warning-no-data");

function updatePageSwitcher(med) {
  // clear swithcer button content
  swUncompleted.innerHTML = "";
  swCompleted.innerHTML = "";

  // declare Uncompleted icon
  const swUncompletedIcon = createImgElm(
    "../../assets/icons/checkbox-symbolic.svg",
    undefined,
    32
  );

  // declare Completed icon
  const swCompletedIcon = createImgElm(
    "../../assets/icons/checkbox-checked-symbolic.svg",
    undefined,
    32
  );

  if (med.matches) {
    // add icon and text to Uncompleted switcher button
    const swUncompletedText = createTextForElm("Belum selesai");
    swUncompleted.appendChild(swUncompletedIcon);
    swUncompleted.appendChild(swUncompletedText);

    // add icon and text to Completed switcher button
    const swCompletedText = createTextForElm("Sudah selesai");
    swCompleted.appendChild(swCompletedIcon);
    swCompleted.appendChild(swCompletedText);
  } else {
    // add icon to Uncompleted switcher button
    swUncompleted.appendChild(swUncompletedIcon);

    // add icon to Completed switcher button
    swCompleted.appendChild(swCompletedIcon);
  }
}

function updateActDialogWidth(med) {
  if (med.matches) {
    actDialog.classList.remove("w-88");
    actDialog.classList.add("w-64");
  } else {
    actDialog.classList.remove("w-64");
    actDialog.classList.add("w-88");
  }
}

function updateBoxedListWidth(med) {
  if (med.matches) {
    boxedListCont.classList.remove("w-88");
    boxedListCont.classList.add("w-64");
  } else {
    boxedListCont.classList.remove("w-64");
    boxedListCont.classList.add("w-88");
  }
}

function updateWarningNoDataWidth(med) {
  if (med.matches) {
    warnNoData.classList.remove("w-88");
    warnNoData.classList.add("w-64");
  } else {
    warnNoData.classList.remove("w-64");
    warnNoData.classList.add("w-88");
  }
}

updatePageSwitcher(winMatchMed);
updateActDialogWidth(winMatchMed);
updateBoxedListWidth(winMatchMed);
updateWarningNoDataWidth(winMatchMed);
winMatchMed.addEventListener("change", function (ev) {
  updatePageSwitcher(ev.target);
  updateActDialogWidth(ev.target);
  updateBoxedListWidth(ev.target);
  updateWarningNoDataWidth(ev.target);
});
