import { Book } from "./data.js";
import {
  saveBookToBookList,
  removeBookFromBookList,
  updateBookData,
  loadBookList,
  activePage,
  createCompletedBookList,
  createNotCompleteBookList,
} from "./storage.js";

const actDialog = document.getElementById("set-book-data-act-dialog");
const actDialogHeader = document.querySelectorAll(".headerbar")[1];
const titleFormCont = document.getElementById("title-form");
const authorFormCont = document.getElementById("author-form");
const yearFormCont = document.getElementById("year-form");

const boxedListCont = document.getElementById("boxed-list");

const warnNoData = document.getElementById("warning-no-data");

function createImgElm(src, alt = "", height) {
  const img = document.createElement("img");
  img.setAttribute("src", `${src}`);
  img.setAttribute("alt", `${alt}`);
  img.setAttribute("height", `${height}`);
  img.style.margin = "0 8px";

  return img;
}

function createTextForElm(text) {
  const txt = document.createElement("span");
  txt.innerText = text;

  return txt;
}

function createButton(text) {
  const button = document.createElement("button");
  button.classList.add("button", "padd", "rounded");
  button.innerText = text;

  return button;
}

function createTextInputForm(label, f, val) {
  // create label for the form
  const lbl = document.createElement("label");
  lbl.setAttribute("for", `${f}`);
  lbl.classList.add("input-label", "padd");
  lbl.innerText = label;

  // create the text input form
  const txt = document.createElement("input");
  txt.setAttribute("type", "text");
  txt.setAttribute("id", `${f}`);
  txt.setAttribute("required", undefined);
  txt.classList.add("text-input", "padd", "bg-l2-grey", "rounded");
  txt.value = val;

  return [lbl, txt];
}

function createNumInputForm(label, f, val) {
  // create label for the form
  const lbl = document.createElement("label");
  lbl.setAttribute("for", `${f}`);
  lbl.classList.add("input-label", "padd");
  lbl.innerText = label;

  // create the number input form
  const txt = document.createElement("input");
  txt.setAttribute("type", "number");
  txt.setAttribute("id", `${f}`);
  txt.setAttribute("required", undefined);
  txt.classList.add("text-input", "padd", "bg-l2-grey", "rounded");
  txt.value = val;

  return [lbl, txt];
}

function buildBookDataForm(
  titleCont,
  titleVal = "",
  authorCont,
  authorVal = "",
  yearCont,
  yearVal = 1970
) {
  const titleFormElm = createTextInputForm("Judul", "title", titleVal);
  for (const elm of titleFormElm) {
    titleCont.appendChild(elm);
  }

  const authorFormElm = createTextInputForm("Penulis", "author", authorVal);
  for (const elm of authorFormElm) {
    authorCont.appendChild(elm);
  }

  const yearFormElm = createNumInputForm("Tahun Cetak", "year", yearVal);
  for (const elm of yearFormElm) {
    yearCont.appendChild(elm);
  }

  return [titleFormElm, authorFormElm, yearFormElm];
}

function hideActDialog() {
  actDialog.style.display = "none";
  actDialogHeader.innerHTML = "";
  titleFormCont.innerHTML = "";
  authorFormCont.innerHTML = "";
  yearFormCont.innerHTML = "";
}

function buildSetBookDataActDialog(act, book = null) {
  // create headerbar elm
  const cancelButton = createButton("Batal");
  cancelButton.classList.add("bg-l2-grey", "black-text");
  cancelButton.addEventListener("click", function (ev) {
    hideActDialog();
  });

  if (act == "add") {
    const dialogTitle = createTextForElm("Tambah Buku");
    dialogTitle.classList.add("headerbar-title");

    const submitButton = createButton("Tambah");
    submitButton.classList.add("bg-blue", "white-text");

    // build headerbar
    actDialogHeader.innerHTML = "";
    actDialogHeader.appendChild(cancelButton);
    actDialogHeader.appendChild(dialogTitle);
    actDialogHeader.appendChild(submitButton);

    // build form
    const bForm = buildBookDataForm(
      titleFormCont,
      undefined,
      authorFormCont,
      undefined,
      yearFormCont,
      undefined
    );

    // make submit button save the data
    submitButton.addEventListener("click", function (ev) {
      const bFormData = {
        t: bForm[0][1].value,
        a: bForm[1][1].value,
        y: bForm[2][1].value,
      };

      const b = new Book(undefined, bFormData.t, bFormData.a, bFormData.y);
      saveBookToBookList(b);
      buildBoxedListItems();
      hideActDialog();
    });
  } else {
    const dialogTitle = createTextForElm("Edit Buku");
    dialogTitle.classList.add("headerbar-title");

    const submitButton = createButton("Simpan");
    submitButton.classList.add("bg-blue", "white-text");

    // build headerbar
    actDialogHeader.innerHTML = "";
    actDialogHeader.appendChild(cancelButton);
    actDialogHeader.appendChild(dialogTitle);
    actDialogHeader.appendChild(submitButton);

    // build form
    const bForm = buildBookDataForm(
      titleFormCont,
      book.title,
      authorFormCont,
      book.author,
      yearFormCont,
      book.year
    );

    // make submit button save the data
    submitButton.addEventListener("click", function (ev) {
      const bFormData = {
        t: bForm[0][1].value,
        a: bForm[1][1].value,
        y: bForm[2][1].value,
      };

      const b = new Book(
        book.id,
        bFormData.t,
        bFormData.a,
        bFormData.y,
        book.isComplete
      );
      updateBookData(book.id, b);
      buildBoxedListItems();
      hideActDialog();
    });
  }
}

function buildBoxedListItems() {
  loadBookList();

  let bookList = [];
  boxedListCont.innerHTML = "";

  if (activePage == "uncomplete") {
    bookList = createNotCompleteBookList();
  } else {
    bookList = createCompletedBookList();
  }

  for (const b of bookList) {
    const itemCont = document.createElement("div");
    itemCont.classList.add(
      "padd",
      "item-container",
      "border-bottom",
      `${b.id}`
    );

    // item info elms
    const itemInfoCont = document.createElement("div");

    const itemTitle = document.createElement("h3");
    itemTitle.innerText = b.title;
    const itemDesc = document.createElement("p");
    itemDesc.innerText = `${b.author} - ${b.year}`;

    itemInfoCont.appendChild(itemTitle);
    itemInfoCont.appendChild(itemDesc);

    itemCont.appendChild(itemInfoCont);

    // item action elms
    const itemActCont = document.createElement("div");
    itemActCont.classList.add("item-container");

    // edit button
    const edtButton = createButton("");
    const edtBtnIcon = createImgElm(
      "../../assets/icons/edit-symbolic.svg",
      undefined,
      16
    );
    edtButton.appendChild(edtBtnIcon);
    edtButton.classList.add("bg-white", "hvr-bg-white");
    edtButton.addEventListener("click", function (ev) {
      buildSetBookDataActDialog("edit", b);
      actDialog.style.display = "block";
    });

    // del button
    const delButton = createButton("");
    const delBtnIcon = createImgElm(
      "../../assets/icons/user-trash-symbolic.svg",
      undefined,
      16
    );
    delButton.appendChild(delBtnIcon);
    delButton.classList.add("bg-white", "hvr-bg-white");
    delButton.addEventListener("click", function (ev) {
      removeBookFromBookList(b.id);
      buildBoxedListItems();
    });

    // status button
    const actButton = createButton("");
    const actBtnIcon = () => {
      if (b.isComplete) {
        return createImgElm(
          "../../assets/icons/checkbox-symbolic.svg",
          undefined,
          16
        );
      } else {
        return createImgElm(
          "../../assets/icons/checkbox-checked-symbolic.svg",
          undefined,
          16
        );
      }
    };
    actButton.appendChild(actBtnIcon());
    actButton.classList.add("bg-white", "hvr-bg-white");
    actButton.addEventListener("click", function (ev) {
      if (b.isComplete) {
        b.makeNotComplete();
        updateBookData(b.id, b);
        buildBoxedListItems();
      } else {
        b.makeComplete();
        updateBookData(b.id, b);
        buildBoxedListItems();
      }
    });

    itemActCont.appendChild(edtButton);
    itemActCont.appendChild(actButton);
    itemActCont.appendChild(delButton);

    itemCont.appendChild(itemActCont);

    // append item container to boxed list container
    boxedListCont.appendChild(itemCont);
  }

  if (bookList.length == 0) {
    warnNoData.style.display = "block";
    boxedListCont.style.display = "none";
  } else {
    warnNoData.style.display = "none";
    boxedListCont.style.display = "block";
  }
}

export {
  createImgElm,
  createTextForElm,
  buildSetBookDataActDialog,
  buildBoxedListItems,
  actDialog,
  boxedListCont,
};
