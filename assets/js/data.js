class Book {
  constructor(id = Date.now(), title, author, year, status = false) {
    this.id = id;
    this.title = title;
    this.author = author;
    this.year = Number(`${year}`);
    this.isComplete = status;
  }

  makeComplete() {
    this.isComplete = true;
  }

  makeNotComplete() {
    this.isComplete = false;
  }

  setTitle(title) {
    this.title = title;
  }

  setAuthor(author) {
    this.author = author;
  }

  setYear(year) {
    this.year = year;
  }
}

export { Book };
