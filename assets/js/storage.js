import { Book } from "./data.js";

// storage key
const BOOKS = "books";
const ACTIVE_PAGE = "active-page";

// book list to be stored
let bookList = [];

// book list to be used when building ui
let completedBookList = [];
let uncompletedBookList = [];

// page swither state
let activePage = "uncomplete";

function createCompletedBookList() {
  let l = [];

  for (const book of bookList) {
    if (book.isComplete) {
      l.push(book);
    }
  }

  return l;
}

function createNotCompleteBookList() {
  let l = [];

  for (const book of bookList) {
    if (!book.isComplete) {
      l.push(book);
    }
  }

  return l;
}

function loadActivePage() {
  const page = localStorage.getItem(ACTIVE_PAGE);
  if (page == null) {
    localStorage.setItem(ACTIVE_PAGE, activePage);
  } else {
    activePage = page;
  }
}

function saveActivePage(pg) {
  localStorage.setItem(ACTIVE_PAGE, pg);
}

// load prev data if any
function loadBookList() {
  bookList = [];

  const data = JSON.parse(localStorage.getItem(BOOKS));
  if (data == null) {
    completedBookList = [];
    uncompletedBookList = [];
  }

  if (data != 0 && data != null) {
    for (const book of data) {
      const b = new Book(
        book.id,
        book.title,
        book.author,
        book.year,
        book.isComplete
      );
      bookList.push(b);
    }

    completedBookList = createCompletedBookList();
    uncompletedBookList = createNotCompleteBookList();
  }
}

function saveBookToBookList(book) {
  bookList.push(book);

  localStorage.setItem(BOOKS, JSON.stringify(bookList));

  loadBookList();
}

function findBookIndex(bookId) {
  for (const index in bookList) {
    if (bookId == bookList[index].id) {
      return index;
    }
  }
}

function removeBookFromBookList(bookId) {
  const target = findBookIndex(bookId);
  bookList.splice(target, 1);

  localStorage.setItem(BOOKS, JSON.stringify(bookList));

  loadBookList();
}

function updateBookData(bookId, book) {
  const target = findBookIndex(bookId);
  bookList.splice(target, 1, book);

  localStorage.setItem(BOOKS, JSON.stringify(bookList));

  loadBookList();
}

function delBookData() {
  const msg =
    "Tindakan ini akan menghapus semua data web ini yang tersimpan pada browser Anda!";

  const consent = window.confirm(msg);

  if (consent) {
    localStorage.clear();
    window.location.reload();
  }

  loadBookList();
}

export {
  saveBookToBookList,
  removeBookFromBookList,
  updateBookData,
  loadBookList,
  delBookData,
  loadActivePage,
  saveActivePage,
  createCompletedBookList,
  createNotCompleteBookList,
  completedBookList,
  uncompletedBookList,
  activePage,
};
