# Front-end web project

Project ini merupakan project submission pada kelas dicoding belajar membuat front-end web untuk pemula.

[![Netlify Status](https://api.netlify.com/api/v1/badges/be530e01-cc24-4c02-b398-c36409c6fa7e/deploy-status)](https://app.netlify.com/sites/hrzn-a315/deploys)

## Kriteria Submission

Buatlah aplikasi web yang dapat memasukan data buku ke dalam rak, memindahkan data buku antar rak, dan menghapus data buku dari rak.

Untuk lebih jelasnya, terdapat 5 kriteria utama pada Bookshelf Apps yang harus Anda buat.

### Kriteria 1: Mampu Menambahkan Data Buku

- Bookshelf Apps harus mampu **menambahkan data buku baru**.
- Data buku yang disimpan merupakan objek JavaScript dengan struktur berikut:

```javascript
{
    id: string | number,
    title: string,
    author: string,
    year: number,
    isComplete: boolean,
}
```

### Kriteria 2: Memiliki Dua Rak Buku

- Rak buku "Belum selesai dibaca" hanya menyimpan buku jika properti `isComplete` bernilai `false`.
- Rak buku "Selesai dibaca" hanya menyimpan buku jika properti `isComplete` bernilai `true`.

### Kriteria 3: Dapat Memindahkan Buku antar Rak

- Buku yang ditampilkan pada rak, baik itu "Belum selesai dibaca" maupun "Selesai dibaca" harus dapat dipindahkan di antara keduanya.

### Kriteria 4: Dapat Menghapus Data Buku

- Buku yang ditampilkan pada rak, baik itu "Belum selesai dibaca" maupun "Selesai dibaca" **harus dapat dihapus**.

### Kriteria 5: Manfaatkan localStorage dalam Menyimpan Data Buku

- Data buku yang ditampilkan pada rak, baik itu "Belum selesai dibaca" maupun "Selesai dibaca" **harus dapat bertahan walaupun halaman web ditutup**.
- Dengan begitu, Anda harus menyimpan data buku pada **localStorage**.

## Saran - saran untuk mendapat nilai tinggi

- Tambahkan fitur pencarian untuk mem-filter buku yang ditampilkan pada rak sesuai dengan **title** buku yang dituliskan pada kolom pencarian.
- Berkreasilah dengan membuat proyek Bookshelf Apps **tanpa menggunakan project starter**.
- Menuliskan kode dengan bersih.
  - Bersihkan comment dan kode yang tidak digunakan.
  - Indentasi yang sesuai.
- Terdapat improvisasi fitur seperti (pilih satu):
  - Custom Dialog ketika menghapus buku.
  - Dapat edit buku.
  - dsb.
